<?php
/*
  Plugin Name: Gutenberg test case for issue 23317
  Plugin URI: https://gitlab.com/cpl/cpl-gutenberg-23317
  Description: test case for https://github.com/WordPress/gutenberg/issues/23317
  Version: 0.0.1
  Author: Will Skora
  License: GPL 3.0+
 */

namespace cpl\gutenberg_23317;
// bug report for
// changes the upload path for media when post type of page is open
// within Gutenberg/block-editor
// upload path is changed to /uploads/board/
// make sure that a directory named board is already made in uploads folder

function cpl_filter_upload_path( $args ) {

	// Get the current post_id
	$id = ( isset( $_POST['post_id'] ) ? $_POST['post_id'] : '' );

	//
	if ( get_post_type( $id ) === 'page' ) {
		$newdir = '/' . 'board';

		$args['path']   = str_replace( $args['subdir'], '', $args['path'] ); //remove default subdir
		$args['url']    = str_replace( $args['subdir'], '', $args['url'] );
		$args['subdir'] = $newdir;
		$args['path']  .= $newdir;
		$args['url']   .= $newdir;
	}
	return $args;
}
add_filter( 'upload_dir', __NAMESPACE__ . '\cpl_filter_upload_path' );
